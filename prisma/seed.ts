import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
  const flats = await prisma.flat.createMany({
    data: [
      {
        squareMeters: 80,
        name: 'Cozy Apartment',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 1200.5,
        numberOfRooms: 2,
        floor: 3,
        isBalconyInclude: true,
        materials: 'Brick',
        status: 'Available',
      },
      {
        squareMeters: 95,
        name: 'Spacious Loft',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 1800.75,
        numberOfRooms: 3,
        floor: 5,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 70,
        name: 'Modern Studio',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 950.25,
        numberOfRooms: 1,
        floor: 2,
        isBalconyInclude: true,
        materials: 'Concrete',
        status: 'Rented',
      },
      {
        squareMeters: 110,
        name: 'Luxury Penthouse',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 2800.99,
        numberOfRooms: 4,
        floor: 12,
        isBalconyInclude: true,
        materials: 'Glass',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
      {
        squareMeters: 60,
        name: 'Charming Cottage',
        location: {
          lat: 1,
          lang: 1,
        },
        price: 800.0,
        numberOfRooms: 2,
        floor: 1,
        isBalconyInclude: false,
        materials: 'Wood',
        status: 'Available',
      },
    ],
  });
  console.log('Created restaurants:', flats.count);
}
main()
  .then(() => prisma.$disconnect())
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
