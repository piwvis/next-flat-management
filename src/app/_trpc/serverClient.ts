import { appRouter } from '@/server';
import { httpBatchLink } from '@trpc/client';

const url =
  process.env.NODE_ENV === 'production'
    ? 'https://next-flat-management-4ze1.vercel.app/api/trpc'
    : 'http://localhost:3000/api/trpc';

export const serverClient = appRouter.createCaller({
  links: [httpBatchLink({ url })],
});
