import Header from '@/components/layout/header';
import { PropsWithChildren } from 'react';

export default async function HomeLayout({ children }: PropsWithChildren) {
  return (
    <main className='container'>
      <Header />
      {children}
    </main>
  );
}
