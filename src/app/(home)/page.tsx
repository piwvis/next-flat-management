import Flats from './components/flats';

export default async function Home() {
  return (
    <div className='container mt-4'>
      <Flats />
    </div>
  );
}
