import { create } from 'zustand';

export const useFilters = create<{
  price: number;
  numberOfRooms: number;
  setFilters: (values: { price: number; numberOfRooms: number }) => void;
}>((set) => ({
  price: 10,
  numberOfRooms: 1,
  setFilters: (values) =>
    set({ price: values.price, numberOfRooms: values.numberOfRooms }),
}));
