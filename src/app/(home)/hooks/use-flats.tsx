import { api } from '@/app/utils/provider';
import { useToast } from '@/components/ui/use-toast';
import { useQueryClient } from '@tanstack/react-query';
import { getQueryKey } from '@trpc/react-query';

export const useFlatDelete = () => {
  const { toast } = useToast();
  const queryClient = useQueryClient();

  const mutation = api.deleteFlat.useMutation({
    mutationKey: ['deleteFlat'],
    onSuccess() {
      queryClient.invalidateQueries({ queryKey: [getQueryKey(api.flatsList)] });
      toast({
        title: 'Your flat was deleted',
        variant: 'success',
      });
    },
    onError() {
      toast({
        title: 'You must be logged in',
        variant: 'destructive',
      });
    },
  });
  return mutation;
};

export const useChangeFlatStatus = () => {
  const { toast } = useToast();
  const queryClient = useQueryClient();

  const query = api.changeFlatStatus.useMutation({
    mutationKey: ['changeFlatStatus'],
    onSuccess() {
      queryClient.invalidateQueries({ queryKey: [getQueryKey(api.flatsList)] });
      toast({
        title: 'Your flat status was changed',
        variant: 'success',
      });
    },
    onError() {
      toast({
        title: 'You must be logged in',
        variant: 'destructive',
      });
    },
  });
  return query;
};

export const useCreateFlat = () => {
  const { toast } = useToast();
  const queryClient = useQueryClient();

  const mutation = api.addFlat.useMutation({
    mutationKey: ['createFlat'],
    onSuccess() {
      queryClient.invalidateQueries({ queryKey: [getQueryKey(api.flatsList)] });
      toast({
        title: 'Your flat was successfully created',
        variant: 'success',
      });
    },
    onError() {
      toast({
        title: 'You must be logged in',
        variant: 'destructive',
      });
    },
  });
  return mutation;
};

export const useEditFlat = () => {
  const { toast } = useToast();
  const queryClient = useQueryClient();

  const mutation = api.updateFlat.useMutation({
    mutationKey: ['updateFlat'],
    onSuccess() {
      queryClient.invalidateQueries({ queryKey: [getQueryKey(api.flatsList)] });
      toast({
        title: 'Your flat was successfully edited',
        variant: 'success',
      });
    },
    onError() {
      toast({
        title: 'You must be logged in',
        variant: 'destructive',
      });
    },
  });
  return mutation;
};

export const useGetFlats = ({
  price,
  numberOfRooms,
}: {
  numberOfRooms: number;
  price: number;
}) => {
  const queryClient = useQueryClient();
  const query = api.flatsList.useQuery(
    { price, numberOfRooms },
    {
      queryKey: ['flatsList', { price, numberOfRooms }],
      onSuccess() {
        queryClient.invalidateQueries({
          queryKey: [getQueryKey(api.flatsList), price, numberOfRooms],
        });
      },
    }
  );
  return query;
};

export const useGetFlatByID = (id: number) => {
  const query = api.flatByID.useQuery({ id: id });
  return query;
};
