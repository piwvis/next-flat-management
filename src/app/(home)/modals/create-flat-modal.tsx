import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from '@/components/ui/sheet';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { useForm } from 'react-hook-form';
import * as z from 'zod';
import { Switch } from '@/components/ui/switch';
import { useCreateFlat } from '../hooks/use-flats';
import { formFlatSchema } from '@/lib/schemas/form-flat-schema';

export function CreateFlatModal() {
  const { mutate: createFlat } = useCreateFlat();

  type FormData = z.infer<typeof formFlatSchema>;

  const form = useForm<FormData>({
    resolver: zodResolver(formFlatSchema),
    defaultValues: {
      name: 'Name',
      floor: 1,
      squareMeters: 10,
      isBalconyInclude: true,
      materials: 'Wood; Stone',
      status: 'Available',
      price: 10,
      numberOfRooms: 1,
      location: { lat: 1, lang: 1 },
    },
  });

  function onSubmit(values: FormData) {
    createFlat(values);
  }
  return (
    <Sheet>
      <SheetTrigger asChild>
        <Button variant='outline'>Create Flat</Button>
      </SheetTrigger>
      <SheetContent className='overflow-y-scroll'>
        <SheetHeader>
          <SheetTitle>Enter flat values</SheetTitle>
          <SheetDescription>Specify flat characteristics</SheetDescription>
        </SheetHeader>
        <div className='grid gap-4 py-4'>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className='space-y-8'>
              <FormField
                control={form.control}
                name='name'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Name</FormLabel>
                    <FormControl>
                      <Input
                        id='name'
                        placeholder={`Enter Flat Name`}
                        onChange={(event) => onChange(event.target.value)}
                        value={value}
                        type='text'
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='floor'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Floor</FormLabel>
                    <FormControl>
                      <Input
                        id='floor'
                        placeholder={`Enter Flat Floor`}
                        value={value}
                        type='number'
                        onChange={(event) => onChange(event.target.value)}
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='squareMeters'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Square Meters</FormLabel>
                    <FormControl>
                      <Input
                        id='squareMeters'
                        placeholder={`Enter Square Meters`}
                        value={value}
                        type='number'
                        onChange={(event) => onChange(event.target.value)}
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='materials'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Materials</FormLabel>
                    <FormControl>
                      <Input
                        id='materials'
                        placeholder={`Enter Flat Materials`}
                        value={value}
                        onChange={(event) => onChange(event.target.value)}
                        type='text'
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='status'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Status</FormLabel>
                    <FormControl>
                      <Input
                        id='status'
                        placeholder={`Enter Flat Status`}
                        value={value}
                        onChange={(event) => onChange(event.target.value)}
                        type='text'
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='price'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Price</FormLabel>
                    <FormControl>
                      <Input
                        id='price'
                        placeholder={`Enter Price`}
                        value={value}
                        type='number'
                        onChange={(event) => onChange(event.target.value)}
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='numberOfRooms'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Rooms</FormLabel>
                    <FormControl>
                      <Input
                        id='numberOfRooms'
                        placeholder={`Enter Number of Rooms`}
                        value={value}
                        type='number'
                        onChange={(event) => onChange(event.target.value)}
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='isBalconyInclude'
                render={({ field }) => (
                  <FormItem className='flex flex-row items-center justify-between rounded-lg border p-3 shadow-sm'>
                    <div className='space-y-0.5'>
                      <FormLabel>Is Balcony Include</FormLabel>
                    </div>
                    <FormControl>
                      <Switch
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
              <div className='flex gap-2'>
                <FormField
                  control={form.control}
                  name='location.lat'
                  render={({ field: { value, onChange, ...field } }) => (
                    <FormItem>
                      <FormLabel>Latitude</FormLabel>
                      <FormControl>
                        <Input
                          id='location.lat'
                          placeholder={`Enter Flat Latitude`}
                          value={value}
                          type='number'
                          onChange={(event) => onChange(event.target.value)}
                          {...field}
                        />
                      </FormControl>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='location.lang'
                  render={({ field: { value, onChange, ...field } }) => (
                    <FormItem>
                      <FormLabel>Longitude</FormLabel>
                      <FormControl>
                        <Input
                          id='location.lang'
                          placeholder={`Enter Flat Longitude`}
                          value={value}
                          type='number'
                          onChange={(event) => onChange(event.target.value)}
                          {...field}
                        />
                      </FormControl>

                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <SheetClose asChild>
                <Button disabled={!form.formState.isValid} type='submit'>
                  Create Flat
                </Button>
              </SheetClose>
            </form>
          </Form>
        </div>
      </SheetContent>
    </Sheet>
  );
}
