import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from '@/components/ui/sheet';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { useFilters } from '../hooks/use-filters';
import { useForm } from 'react-hook-form';
import { formFlatFilterSchema } from '@/lib/schemas/form-flat-schema';
import z from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';

export function FilterModal() {
  const { price, numberOfRooms, setFilters } = useFilters();

  type FormData = z.infer<typeof formFlatFilterSchema>;

  const form = useForm<FormData>({
    resolver: zodResolver(formFlatFilterSchema),
    defaultValues: {
      price: price,
      numberOfRooms: numberOfRooms,
    },
  });

  function onSubmit(values: FormData) {
    setFilters(values);
  }
  return (
    <Sheet>
      <SheetTrigger asChild>
        <Button variant='outline'>Filter Flats</Button>
      </SheetTrigger>
      <SheetContent>
        <SheetHeader>
          <SheetTitle>Adjust flats values</SheetTitle>
          <SheetDescription>
            Equal or greater than price AND rooms
          </SheetDescription>
        </SheetHeader>
        <div className='grid gap-4 py-4'>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className='space-y-8'>
              <FormField
                control={form.control}
                name='price'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Price</FormLabel>
                    <FormControl>
                      <Input
                        id='price'
                        placeholder={`Enter Price`}
                        type='number'
                        value={value}
                        onChange={(event) => onChange(event.target.value)}
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='numberOfRooms'
                render={({ field: { value, onChange, ...field } }) => (
                  <FormItem>
                    <FormLabel>Rooms</FormLabel>
                    <FormControl>
                      <Input
                        id='numberOfRooms'
                        placeholder={`Enter Number of Rooms`}
                        value={value}
                        type='number'
                        onChange={(event) => onChange(event.target.value)}
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <SheetClose asChild>
                <Button type='submit'>Filter</Button>
              </SheetClose>
            </form>
          </Form>
        </div>
      </SheetContent>
    </Sheet>
  );
}
