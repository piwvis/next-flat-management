'use client';

import { ColumnDef, Row } from '@tanstack/react-table';
import { MoreHorizontal } from 'lucide-react';
import { Button } from '@/components/ui/button';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { useChangeFlatStatus, useFlatDelete } from '../hooks/use-flats';
import { JsonArray, JsonObject } from '@prisma/client/runtime/library';
import { EditFlatModal } from '../modals/edit-flat-modal';
import { useState } from 'react';

export type TDataFlat = {
  id: number;
  name: string;
  status: string;
  price: number;
  numberOfRooms: number;
  floor: number;
  materials: string;
  createdAt: string;
  updatedAt: string;
  squareMeters: number;
  location:
    | {
        lat: number;
        lang: number;
      }
    | string
    | number
    | boolean
    | null
    | JsonObject
    | JsonArray;
  isBalconyInclude: boolean;
  userId: string | null;
};

export const columns: ColumnDef<TDataFlat>[] = [
  {
    accessorKey: 'id',
    header: 'ID',
  },
  {
    accessorKey: 'name',
    header: 'Name',
  },
  {
    accessorKey: 'status',
    header: 'Status',
  },
  {
    accessorKey: 'price',
    header: 'Price',
  },
  {
    accessorKey: 'numberOfRooms',
    header: 'Rooms',
  },
  {
    id: 'actions',
    header: 'Actions',
    cell: ({ row }) => ColumnsActions(row),
  },
];

export const ColumnsActions = (row: Row<TDataFlat>) => {
  const flat = row.original;
  const { mutate: deleteFlat, isLoading: isLoadingDelete } = useFlatDelete();
  const { mutate: changeStatus } = useChangeFlatStatus();
  const [field, setSelectField] = useState(flat.status);

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant='ghost' className='h-8 w-8 p-0'>
          <span className='sr-only'>Open menu</span>
          <MoreHorizontal className='h-4 w-4' />
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align='end'>
        <DropdownMenuLabel>Actions</DropdownMenuLabel>
        <DropdownMenuItem>
          <Select
            defaultValue={field}
            onValueChange={(value) => {
              setSelectField(value);
              changeStatus({ id: flat.id, status: value });
            }}
          >
            <SelectTrigger className='w-[180px]'>
              <SelectValue placeholder='Select a status' />
            </SelectTrigger>
            <SelectContent>
              <SelectGroup>
                <SelectItem value='Rent'>Rent</SelectItem>
                <SelectItem value='Available'>Available</SelectItem>
              </SelectGroup>
            </SelectContent>
          </Select>
        </DropdownMenuItem>
        <DropdownMenuSeparator />
        <EditFlatModal {...flat} />
        <DropdownMenuItem
          disabled={isLoadingDelete}
          onClick={() => deleteFlat({ id: flat.id })}
        >
          Delete
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
