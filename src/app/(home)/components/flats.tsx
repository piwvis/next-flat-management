'use client';

import { Button } from '@/components/ui/button';
import { useFilters } from '../hooks/use-filters';
import { useGetFlats } from '../hooks/use-flats';
import { CreateFlatModal } from '../modals/create-flat-modal';
import { FilterModal } from '../modals/filter-modal';
import { columns } from './columns';
import { FlatsTable } from './flats-table';
import { User } from 'next-auth';
import { Tabs, TabsList, TabsTrigger, TabsContent } from '@/components/ui/tabs';
import { useState } from 'react';

type UserProps = {
  user?: Pick<User, 'id'>;
};

const Flats = (user: UserProps) => {
  const { price, numberOfRooms } = useFilters();
  const [pageIndex, setPageIndex] = useState(0);

  const { data: flats, isLoading } = useGetFlats({
    numberOfRooms: numberOfRooms,
    price: price,
  });

  if (isLoading)
    return (
      <div className='border-background-600 animate-pulse rounded-xl border-4 px-5 py-32'></div>
    );

  return (
    <div>
      <div className='my-2 flex w-full gap-2'>
        <FilterModal />
        {user ? (
          <CreateFlatModal />
        ) : (
          <Button disabled={true} variant='outline'>
            Create Flat
          </Button>
        )}
      </div>
      {flats && <FlatsTable columns={columns} data={flats} />}
    </div>
  );
};

export default Flats;
