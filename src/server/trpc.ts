import { authOptions } from '@/lib/auth/auth';
import { TRPCError, initTRPC } from '@trpc/server';
import { getServerSession } from 'next-auth';
// Avoid exporting the entire t-object
// since it's not very descriptive.
// For instance, the use of a t variable
// is common in i18n libraries.
const t = initTRPC.create();

// Base router and procedure helpers
export const router = t.router;
const isAuthed = t.middleware(async (opts) => {
  const session = await getServerSession(authOptions);
  if (!session?.user) {
    throw new TRPCError({ code: 'UNAUTHORIZED' });
  }
  return opts.next({
    ctx: {
      user: session.user,
    },
  });
});
export const publicProcedure = t.procedure;
export const protectedProcedure = t.procedure.use(isAuthed);
