import { db } from '@/lib/db';
import { protectedProcedure, publicProcedure, router } from './trpc';
import {
  flatSchema,
  formFlatSchema,
  idSchema,
  updateFlatSchema,
  updateFlatStatusSchema,
} from '@/lib/schemas/form-flat-schema';

export const appRouter = router({
  flatsList: publicProcedure.input(flatSchema).query(async (req) => {
    return await db.flat.findMany({
      where: {
        AND: [
          {
            price: {
              gte: req.input.price,
            },
          },
          {
            numberOfRooms: {
              gte: req.input.numberOfRooms,
            },
          },
        ],
      },
    });
  }),

  paginateFlats: publicProcedure.query(async () => {
    return await db.flat.findMany({
      skip: 3,
      take: 4,
    });
  }),

  flatByID: publicProcedure.input(idSchema).query(async (req) => {
    return await db.flat.findUnique({
      where: { id: req.input.id },
    });
  }),

  addFlat: protectedProcedure.input(formFlatSchema).mutation(async (req) => {
    return await db.flat.create({
      data: {
        ...req.input,
        userId: req.ctx.user?.id,
      },
    });
  }),

  updateFlat: protectedProcedure
    .input(updateFlatSchema)
    .mutation(async (req) => {
      return await db.flat.update({
        where: {
          id: req.input.id,
        },
        data: {
          ...req.input.flat,
        },
      });
    }),

  deleteFlat: protectedProcedure.input(idSchema).mutation(async (req) => {
    return await db.flat.delete({
      where: {
        id: req.input.id,
      },
    });
  }),

  changeFlatStatus: protectedProcedure
    .input(updateFlatStatusSchema)
    .mutation(async (req) => {
      return await db.flat.update({
        where: {
          id: req.input.id,
        },
        data: {
          status: req.input.status,
        },
      });
    }),
});
// Export type router type signature,
// NOT the router itself.
export type AppRouter = typeof appRouter;
