import House from '@/components/icons/house';
import SignInModal from './components/sign-in-modal';
import { getAuthSession } from '@/lib/auth/auth';
import Image, { StaticImageData } from 'next/image';
import Link from 'next/link';

const Header = async () => {
  const session = await getAuthSession();

  return (
    <div className='mt-4 flex w-full items-center justify-between rounded-lg bg-black p-2 px-4 text-white'>
      <div className='flex items-center justify-between gap-2  align-baseline'>
        <House className='h-8 w-8' />
        <h2 className='scroll-m-20 text-sm font-semibold tracking-tight transition-colors first:mt-0 sm:text-2xl'>
          Flat Management
        </h2>
      </div>
      {session ? (
        <div className='flex items-center gap-2'>
          {session.user.name}
          {session.user.image && (
            <Image
              width={30}
              height={30}
              className='rounded'
              src={session.user?.image}
              alt={''}
            />
          )}
          <Link
            className='rounded-md bg-white p-1 text-black'
            href={'/api/auth/signout'}
          >
            Sign Out
          </Link>
        </div>
      ) : (
        <SignInModal />
      )}
    </div>
  );
};

export default Header;
