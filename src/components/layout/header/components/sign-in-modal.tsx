import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '@/components/ui/dialog';
import SignIn from '@/lib/auth/components/sign-in';

const SignInModal = () => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button className='bg-white text-sm text-black hover:bg-slate-300 sm:text-lg'>
          Sign In
        </Button>
      </DialogTrigger>
      <DialogContent className='flex flex-col justify-center text-center text-sm sm:text-lg'>
        <DialogHeader>
          <DialogTitle className='text-sm sm:text-lg'>Sign In</DialogTitle>
          <DialogDescription className='text-sm sm:text-lg'>
            Authenficate using Google
          </DialogDescription>
        </DialogHeader>
        <SignIn />
      </DialogContent>
    </Dialog>
  );
};

export default SignInModal;
