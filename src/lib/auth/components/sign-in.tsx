'use client';

import { Button } from '@/components/ui/button';
import { useToast } from '@/components/ui/use-toast';
import { signIn } from 'next-auth/react';
import { useState } from 'react';

const SignIn = ({}) => {
  const [isLoading, setLoading] = useState(false);
  const { toast } = useToast();

  const loginWithGoogle = async () => {
    setLoading(true);
    try {
      await signIn('google');
    } catch (err) {
      toast({
        title: 'There was a problem',
        description: 'There was an error logging with Google',
        variant: 'destructive',
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <Button onClick={loginWithGoogle}>
        {!isLoading ? 'Google' : 'Loading...'}
      </Button>
    </div>
  );
};

export default SignIn;
