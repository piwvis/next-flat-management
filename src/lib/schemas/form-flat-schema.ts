import * as z from 'zod';

export const formFlatSchema = z.object({
  name: z
    .string({
      required_error: 'Please write name.',
    })
    .min(4),
  price: z.coerce.number().min(1),
  squareMeters: z.coerce.number().min(1),
  floor: z.coerce.number(),
  materials: z.string(),
  status: z.string(),
  numberOfRooms: z.coerce.number(),
  isBalconyInclude: z.boolean(),
  location: z.object({ lat: z.coerce.number(), lang: z.coerce.number() }),
});

export const formFlatFilterSchema = z.object({
  price: z.coerce.number().min(1),
  numberOfRooms: z.coerce.number(),
});

export const idSchema = z.object({ id: z.number() });

export const flatSchema = z.object({
  name: z.string().optional(),
  price: z.number().optional(),
  floor: z.number().optional(),
  materials: z.string().optional(),
  status: z.string().optional(),
  numberOfRooms: z.number().optional(),
});

export const updateFlatSchema = z.object({
  id: z.number(),
  flat: formFlatSchema,
});

export const updateFlatStatusSchema = z.object({
  id: z.number(),
  status: z.string(),
});
